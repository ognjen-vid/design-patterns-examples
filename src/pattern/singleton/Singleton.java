package pattern.singleton;

public class Singleton {

	// Lazy instantiation to create the singleton; as a result, the singleton
	// instance is not created until the getInstance() method is called for the
	// first time. This technique ensures that singleton instances are created only
	// when needed
	private static Singleton instance = null;

	//Clients cannot instantiate Singleton instances
	//private cannot be subclassed
	//We can change it to public
	private Singleton() {
	}

	//Not thread-safe
	//We can put "synchronized" to make it safe
	
	//Every other time we instantiate using this method
	//it will point to an existing object
	//since instance is static
	public static Singleton getInstance() {
		if (instance == null)
			instance = new Singleton();

		return instance;
	}
}
