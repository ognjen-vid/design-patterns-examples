package pattern.command;

//Concrete Command
public class SellCurrency implements Transaction {
	
	private Currency currency;

	public SellCurrency(Currency currency){
		this.currency = currency;
	}

	public void execute() {
		currency.sell();
	}
}
