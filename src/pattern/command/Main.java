package pattern.command;

public class Main {

	public static void main(String[] args) {

		ExchangeOffice office = new ExchangeOffice();
		
		Currency eur = new Currency();
		
		BuyCurrency buyEur = new BuyCurrency(eur);
		office.requestTransaction(buyEur);
		
		SellCurrency sellEur = new SellCurrency(eur);
		office.requestTransaction(sellEur);

		office.executeTransactions();
	}

}
