package pattern.command;

//Concrete Command
public class BuyCurrency implements Transaction {

	private Currency currency;

	public BuyCurrency(Currency currency) {
		this.currency = currency;
	}

	public void execute() {
		currency.buy();
	}

}
