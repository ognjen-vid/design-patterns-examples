package pattern.command;

//Command Interface
public interface Transaction {

	void execute();
}
