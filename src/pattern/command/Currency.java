package pattern.command;

//Request class
public class Currency {
	private String name = "EUR";
	private int quantity = 10;

	public void buy(){
	      System.out.println("Currency [ Name: " + name + ",  Quantity: " + quantity +" ] bought");
	   }

	public void sell(){
	      System.out.println("Currency [ Name: " + name + ",  Quantity: " + quantity +" ] sold");
	   }
}
