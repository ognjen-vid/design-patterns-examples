package pattern.command;

import java.util.ArrayList;
import java.util.List;

//Invoker
public class ExchangeOffice {
	private List<Transaction> transactionsList = new ArrayList<>(); 

	public void requestTransaction(Transaction tr){
		transactionsList.add(tr);		
	}

	public void executeTransactions(){
		for (Transaction tr : transactionsList) {
			tr.execute();
		}
		transactionsList.clear();
	}
}
