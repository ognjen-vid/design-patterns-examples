package pattern.builder;

/* A customer ordering a lunch. */
public class Main {

	public static void main(String[] args) {
		
		LunchDirector ld = new LunchDirector();
		LunchBuilder heavy = new HeavyLunchBuilder();

		ld.createLunch(heavy);
		

	}

}
