package pattern.builder;

/* "ConcreteBuilder" */
public class LightLunchBuilder extends LunchBuilder {

	public void buildMain() {
		lunch.setMain("Boiled Chicken");
	}

	public void buildSide() {
		lunch.setSide("Rise");
	}

	public void buildDrink() {
		lunch.setDrink("Water");
	}

}
