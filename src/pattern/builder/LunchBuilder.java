package pattern.builder;

/* "Abstract Builder" */
public abstract class LunchBuilder {

	protected Lunch lunch;

	public void createNewLunch() {
		lunch = new Lunch();
	}
	
	public abstract void buildMain();
	public abstract void buildSide();
	public abstract void buildDrink();
	
	public Lunch getLunch() {
		return lunch;
	}


}
