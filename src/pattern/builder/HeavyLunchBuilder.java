package pattern.builder;

/* "ConcreteBuilder" */
public class HeavyLunchBuilder extends LunchBuilder {

	public void buildMain() {
		lunch.setMain("Fried Pork");
	}

	public void buildSide() {
		lunch.setSide("Potatoes+Sauce");
	}

	public void buildDrink() {
		lunch.setDrink("Beer");
	}

}
