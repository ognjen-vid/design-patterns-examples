package pattern.builder;

/* "Product" */
public class Lunch {
	private String main = "";
	private String side = "";
	private String drink = "";

	public void setMain(String main) {
		this.main = main;
	}

	public void setSide(String side) {
		this.side = side;
	}

	public void setDrink(String drink) {
		this.drink = drink;
	}
}
