package pattern.builder;

/* "Director" */
public class LunchDirector {
	
	public Lunch createLunch(LunchBuilder lb) {
		lb.createNewLunch();
		lb.buildMain();
		lb.buildSide();
		lb.buildDrink();
		return lb.getLunch();
	}
}
