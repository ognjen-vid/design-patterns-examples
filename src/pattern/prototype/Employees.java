package pattern.prototype;

import java.util.ArrayList;
import java.util.List;

public class Employees implements Cloneable {
	private List<String> empList;

	public Employees() {
		empList = new ArrayList<String>();
	}

	public Employees(List<String> list) {
		this.empList = list;
	}

	public void loadData() {
		// read all employees from database and put into the list
		empList.add("Pera");
		empList.add("Mika");
		empList.add("Sima");
	}

	public List<String> getEmployeesList() {
		return empList;
	}

	// Whether to use shallow (copy reference) or deep copy (copy everything
	// in a new Object) of the Object properties depends on the requirements and its
	// a design decision
	// Here, we are using deep copy
	@Override
	public Object clone() throws CloneNotSupportedException {
		List<String> temp = new ArrayList<String>();
		for (String s : this.getEmployeesList()) {
			temp.add(s);
		}
		return new Employees(temp);
	}
}
