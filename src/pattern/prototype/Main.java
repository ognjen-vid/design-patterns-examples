package pattern.prototype;

import java.util.List;

public class Main {

	public static void main(String[] args) throws CloneNotSupportedException {
		Employees empsOrig = new Employees();
		empsOrig.loadData();

		// Use the clone method to get the Employee object
		Employees empsNew1 = (Employees) empsOrig.clone();
		Employees empsNew2 = (Employees) empsOrig.clone();
		
		List<String> list = empsNew1.getEmployeesList();
		list.add("Zika");
		
		List<String> list1 = empsNew2.getEmployeesList();
		list1.remove("Sima");

		System.out.println("empsOrig List: " + empsOrig.getEmployeesList());
		System.out.println("empsNew1 List: " + list);
		System.out.println("empsNew2 List: " + list1);
	}

}
