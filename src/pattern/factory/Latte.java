package pattern.factory;

public class Latte implements Coffee {

	public void make() {
		System.out.println("Latte made");
	}

}
