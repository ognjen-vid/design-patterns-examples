package pattern.factory;

// Factory - Generates object based on information
public class CoffeeFactory {

	public static Coffee getCoffee(String criteria) {
		if (criteria.equals("small"))
			return new Espresso();
		else if (criteria.equals("medium"))
			return new Latte();
		else if (criteria.equals("big"))
			return new Iced();

		return null;
	}
}
