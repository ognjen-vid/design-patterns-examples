package pattern.factory;

public class Main {
	public static void main(String[] args) {
		// create a small coffee
		Coffee coffee = CoffeeFactory.getCoffee("small");
		coffee.make();

		// create a medium coffee
		coffee = CoffeeFactory.getCoffee("medium");
		coffee.make();

		// create a big coffee
		coffee = CoffeeFactory.getCoffee("big");
		coffee.make();
	}
}
