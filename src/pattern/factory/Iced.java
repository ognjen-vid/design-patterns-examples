package pattern.factory;

public class Iced implements Coffee {

	public void make() {
		System.out.println("Iced made");
	}

}
