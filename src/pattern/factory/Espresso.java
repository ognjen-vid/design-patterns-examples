package pattern.factory;

public class Espresso implements Coffee {

	public void make() {
		System.out.println("Espresso made");
	}

}
