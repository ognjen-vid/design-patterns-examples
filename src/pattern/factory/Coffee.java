package pattern.factory;

public interface Coffee {
	
	public void make();

}
